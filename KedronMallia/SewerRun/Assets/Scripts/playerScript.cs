﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class playerScript : MonoBehaviour {

	public float speed;
	public static GameObject txtLives;
	public static int potion;
	GameObject inv, axeIcon, potNum, pauseMenu;
	Vector2 invPos, axeIconPos;
	bool invOpen, menuOpen, pause, allowMove;
	Button btnBack, btnMusic;
	public static float currenttime;

	Animator anim;

	public static int lives;

	void Start(){
		txtLives = GameObject.Find ("txtLives");
		anim = GetComponent<Animator> ();
		lives = 5;
		txtLives.GetComponent<Text> ().text = "Lives: " + lives;
		enemy.startMove = false;
		potion = 0;
		inv = GameObject.Find ("inventory");
		axeIcon = GameObject.Find ("axeIcon");
		potNum = GameObject.Find ("potNum");
		invOpen = false;
		menuOpen = false;

		btnBack = GameObject.Find ("btnBack").GetComponent<Button> ();
		btnMusic = GameObject.Find ("btnMusic").GetComponent<Button> ();
		pauseMenu = GameObject.Find ("pauseMenu");

		btnBack.onClick.AddListener (backToMenu);
		btnMusic.onClick.AddListener (musicSwitch);

		GameObject.Find("btnBack").SetActive(false);
		GameObject.Find("btnMusic").SetActive(false);

		pause = true;
		allowMove = true;
	}

	void backToMenu(){
		Destroy (GameObject.Find ("musicManager"));
		Destroy (GameObject.Find ("timeHolder"));
		SceneManager.LoadScene ("MainMenu");
	}

	void musicSwitch(){
		musicManager.pauseAudio (pause);
		pause = !pause;
	}

	void Update (){
		if (allowMove) {
			if (Input.GetKey (KeyCode.D)) {
				transform.Translate (Vector2.right * speed);
				anim.SetInteger ("State", 2);
			} else if (Input.GetKey (KeyCode.S)) {
				transform.Translate (Vector2.down * speed);
				anim.SetInteger ("State", 4);
			} else if (Input.GetKey (KeyCode.W)) {
				transform.Translate (Vector2.up * speed);
				anim.SetInteger ("State", 1);
			} else if (Input.GetKey (KeyCode.A)) {
				transform.Translate (Vector2.left * speed);
				anim.SetInteger ("State", 3);
			} else {
				anim.SetInteger ("State", 0);
			}
			if (Input.GetKey (KeyCode.Q)) {
				if (potion > 0) {
					potion = potion - 1;
					lives++;
					txtLives.GetComponent<Text> ().text = "Lives: " + lives;
				}
			}
			if (Input.GetKeyDown (KeyCode.I)) {
				if (!invOpen) {
					inv.transform.position = new Vector3 (inv.transform.position.x, inv.transform.position.y, -6f);
					if (attackScript.attackT) {
						axeIcon.transform.position = new Vector3 (axeIcon.transform.position.x, axeIcon.transform.position.y, -7f);
					}
					if (potion > 0) {
						potNum.GetComponent<Text> ().text = potion.ToString ();
					}
					invOpen = true;
				} else {
					inv.transform.position = new Vector3 (inv.transform.position.x, inv.transform.position.y, -15f);
					axeIcon.transform.position = new Vector3 (axeIcon.transform.position.x, axeIcon.transform.position.y, -15f);
					potNum.GetComponent<Text> ().text = "";
					invOpen = false;
				}
			}
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (!menuOpen) {
				enemy.startMove = false;
				pauseMenu.transform.position = new Vector3 (pauseMenu.transform.position.x, pauseMenu.transform.position.y, -6f);
				btnBack.gameObject.SetActive(true);
				btnMusic.gameObject.SetActive(true);

				menuOpen = true;
				currenttime = timer.currenttime;
				timer.timerstarted = false;
				allowMove = false;
				} else {
				if (activateSpike.enteredArena) {
					enemy.startMove = true;
				}
				pauseMenu.transform.position = new Vector3 (pauseMenu.transform.position.x, pauseMenu.transform.position.y, -15f);
				GameObject.Find("btnBack").SetActive(false);
				GameObject.Find("btnMusic").SetActive(false);

				timer.timerstarted = true;
				menuOpen = false;
				allowMove = true;
			}
		}
	}
}