﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class potionRangeDetection : MonoBehaviour {

	Vector2 potPos;
	Vector2 highlightPos;
	GameObject potHighlight, pot;
	bool check;

	void Start(){
		potPos = GameObject.Find ("potion").transform.position;
		potHighlight = GameObject.Find ("potHighlight");
		highlightPos = potHighlight.transform.position;
		pot = GameObject.Find ("potion");
		check = false;
	}

	void OnTriggerEnter2D(Collider2D other){
		potHighlight.transform.position = new Vector3(potPos.x,potPos.y,-5f);
		check = true;
	}

	void OnTriggerExit2D(Collider2D other){
		potHighlight.transform.position = highlightPos;
		check = false;
	}

	void Update(){
		if (Input.GetKey (KeyCode.E) && check) {
			playerScript.potion++;
			Destroy (pot);
		}
	}
}
