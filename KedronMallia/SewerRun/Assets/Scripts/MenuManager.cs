﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	Button btnPlay, btnExit, btnURL;
	GameObject timeManager;

	// Use this for initialization
	void Start () {
		btnPlay = GameObject.Find ("btnPlay").GetComponent<Button> ();
		btnExit = GameObject.Find ("btnExit").GetComponent<Button> ();
		btnURL = GameObject.Find ("btnURL").GetComponent<Button> ();
		btnPlay.onClick.AddListener (startGame);
		btnExit.onClick.AddListener (exitGame);
		btnURL.onClick.AddListener (openLink);
		timeManager = GameObject.Find ("timeManager");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void openLink(){
		Application.OpenURL ("https://sewerrun.000webhostapp.com/");
	}

	void startGame(){
		Destroy (timeManager);
		SceneManager.LoadScene ("Instructions");
	}

	void exitGame(){
		Application.Quit ();
	}
}
