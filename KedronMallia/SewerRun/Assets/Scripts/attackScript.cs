﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attackScript : MonoBehaviour {

	Animator anim;
	public static bool attackT;
	bool allowAtt;

	void Start(){
		anim = GetComponent<Animator> ();
		attackT = false;
	}

	void Update(){
		if (Input.GetMouseButtonDown (0) && attackT) {
			allowAtt = true;
			anim.Play ("attack");
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (allowAtt) {
			if (other.gameObject.tag.Equals ("enemy1")) {
				enemy1.health--;
				Debug.Log (enemy1.health);
			}
			if (other.gameObject.tag.Equals ("enemy2")) {
				enemy2.health--;
				Debug.Log (enemy2.health);
			}
			allowAtt = false;
		}
	}
}
