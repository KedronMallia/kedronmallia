﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy1 : MonoBehaviour {

	public static int health;
	GameObject enemy;

	// Use this for initialization
	void Start () {
		health = 3;
		enemy = GameObject.Find ("spike");
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			activateSpike.enemyLeft--;
			Destroy (enemy);
		}
	}
}
