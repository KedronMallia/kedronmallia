﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemy : MonoBehaviour {

	private GameObject Player;
	private float speed;
	public static bool startMove;

	private GameObject rb2d;

	// Use this for initialization
	void Start () {
		Player = GameObject.Find("player");
		speed = 130f;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag.Equals ("player")) {
			playerScript.lives--;
			playerScript.txtLives.GetComponent<Text> ().text = "Lives: " + playerScript.lives;
		}
	}

	// Update is called once per frame
	void Update () {
		if (startMove) {
			float step = speed * Time.deltaTime;
			if (Player) {
				this.transform.position = Vector3.MoveTowards (transform.position, Player.transform.position, step * Time.deltaTime);

			} else {
				this.transform.position = Vector3.MoveTowards (transform.position, Player.transform.position, step * Time.deltaTime);     
			}
		}
	}
}
