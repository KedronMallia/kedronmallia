﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour {
	//these are the variables used to contain the text objects
	GameObject txtTime;

	//variables used to show the current time
	public static float currenttime;

	//has the timer started or not?
	public static bool timerstarted;

	public static string elapsedTime;

	// Use this for initialization
	void Start () {
		txtTime = GameObject.Find ("txtTime");
		currenttime = 0f;
		timerstarted = true;
	}

	string showFormattedTime(float thetime){
		float minutes = 0f;
		float seconds = 0f;

		minutes = Mathf.Floor(thetime / 60f);
		seconds = thetime % 60f;

		elapsedTime = "Time: " + minutes.ToString ("00") + ":" + seconds.ToString ("00");

		return elapsedTime;
	}

	// Update is called once per frame
	void Update () {
		if (timerstarted) {
			currenttime += Time.deltaTime;
			txtTime.GetComponent<Text> ().text = showFormattedTime (currenttime);
		}
	}
}
