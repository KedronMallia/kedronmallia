﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class instManager : MonoBehaviour {

	Button btnPlay, btnBack;

	// Use this for initialization
	void Start () {
		btnPlay = GameObject.Find ("btnPlay").GetComponent<Button> ();
		btnBack = GameObject.Find ("btnBack").GetComponent<Button> ();
		btnPlay.onClick.AddListener (startGame);
		btnBack.onClick.AddListener (goBack);
	}

	// Update is called once per frame
	void Update () {

	}

	void startGame(){
		SceneManager.LoadScene ("Level1");
	}

	void goBack(){
		Destroy (GameObject.Find ("musicManager"));
		SceneManager.LoadScene ("MainMenu");
	}
}
