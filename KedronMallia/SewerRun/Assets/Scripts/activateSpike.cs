﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activateSpike : MonoBehaviour {

	GameObject topDoor, bottomDoor, topDoor1, bottomDoor1;
	public static int enemyLeft;
	public static bool enteredArena;

	void Start(){
		enemyLeft = 2;

		topDoor = GameObject.Find ("topDoor");
		bottomDoor = GameObject.Find ("bottomDoor");
		topDoor1 = GameObject.Find ("topDoor1");
		bottomDoor1 = GameObject.Find ("bottomDoor1");

		topDoor.GetComponent<Collider2D> ().enabled = false;
		bottomDoor.GetComponent<Collider2D> ().enabled = false;
		topDoor1.GetComponent<Collider2D> ().enabled = false;
		bottomDoor1.GetComponent<Collider2D> ().enabled = false;

		enteredArena = false;
	}

	void OnTriggerEnter2D(Collider2D other){
		enteredArena = true;
		enemy.startMove = true;
		topDoor.GetComponent<Collider2D> ().enabled = true;
		bottomDoor.GetComponent<Collider2D> ().enabled = true;
		topDoor1.GetComponent<Collider2D> ().enabled = true;
		bottomDoor1.GetComponent<Collider2D> ().enabled = true;

		topDoor.transform.position = new Vector3 (topDoor.transform.position.x, topDoor.transform.position.y, -5f);
		bottomDoor.transform.position = new Vector3 (bottomDoor.transform.position.x, bottomDoor.transform.position.y, -5f);
		topDoor1.transform.position = new Vector3 (topDoor1.transform.position.x, topDoor1.transform.position.y, -5f);
		bottomDoor1.transform.position = new Vector3 (bottomDoor1.transform.position.x, bottomDoor1.transform.position.y, -5f);
	}

	void Update(){
		if (enemyLeft <= 0) {
			topDoor.GetComponent<Collider2D> ().enabled = false;
			bottomDoor.GetComponent<Collider2D> ().enabled = false;
			topDoor1.GetComponent<Collider2D> ().enabled = false;
			bottomDoor1.GetComponent<Collider2D> ().enabled = false;

			topDoor.transform.position = new Vector3 (topDoor.transform.position.x, topDoor.transform.position.y, -15f);
			bottomDoor.transform.position = new Vector3 (bottomDoor.transform.position.x, bottomDoor.transform.position.y, -15f);
			topDoor1.transform.position = new Vector3 (topDoor1.transform.position.x, topDoor1.transform.position.y, -15f);
			bottomDoor1.transform.position = new Vector3 (bottomDoor1.transform.position.x, bottomDoor1.transform.position.y, -15f);
		}
	}
}
