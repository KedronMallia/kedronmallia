﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rangeDetection : MonoBehaviour {

	Vector2 axePos;
	GameObject highlight;
	Vector2 highlightPos;
	GameObject axe;
	GameObject realAxe;
	bool check;

	void Start(){
		axePos = GameObject.Find ("axe").transform.position;
		highlight = GameObject.Find ("highlight");
		highlightPos = highlight.transform.position;
		axe = GameObject.Find ("axe");
		realAxe = GameObject.Find ("axeAttack");
		check = false;
	}

	void OnTriggerEnter2D(Collider2D other){
		highlight.transform.position = new Vector3(axePos.x,axePos.y,-5f);
		check = true;
	}

	void OnTriggerExit2D(Collider2D other){
		highlight.transform.position = highlightPos;
		check = false;
	}

	void Update(){
		if (Input.GetKey (KeyCode.E) && check) {
			attackScript.attackT = true;
			realAxe.transform.position = new Vector3(realAxe.transform.position.x,realAxe.transform.position.y,-5f);
			Destroy (axe);
		}
	}
}
