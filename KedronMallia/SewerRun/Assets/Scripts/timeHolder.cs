﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class timeHolder : MonoBehaviour {

	GameObject txtYourTime;

	void Awake(){
		DontDestroyOnLoad(this.gameObject);
	}

	public static string time;

	void Update(){
		if (time != null) {
			txtYourTime = GameObject.Find ("txtYourTime");
			txtYourTime.GetComponent<Text> ().text = time;
			enemy.startMove = false;
			time = null;
		}
	}

}
