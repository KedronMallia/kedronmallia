﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class sceneMngr : MonoBehaviour {

	Button btnMenu;

	// Use this for initialization
	void Start () {
		btnMenu = GameObject.Find ("btnMenu").GetComponent<Button> ();
		btnMenu.onClick.AddListener (backToMenu);
	}
	void backToMenu(){
		Destroy (GameObject.Find ("musicManager"));
		Destroy (GameObject.Find ("timeHolder"));
		SceneManager.LoadScene ("MainMenu");
	}

}
